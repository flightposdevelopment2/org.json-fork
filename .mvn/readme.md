# Maven Extensions
Since Maven version 3.3.1 it's possible to add core extensions trough an additional extensions.xml 
	(see https://issues.apache.org/jira/browse/MNG-5771) no patching needed, just add an extensions.xml 
	 at ${maven.projectBasedir}/.mvn/extensions.xml to your project.
	 
These extensions add the AWS Wagon, with out them the build will not be able to perform the intital download of the parent POM.xml from the S3 bucket.
