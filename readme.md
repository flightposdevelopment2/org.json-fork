# flightPOS fork of JSON in Java [package org.json]

JSON is a light-weight, language independent, data interchange format.
See http://www.JSON.org/

## To build
### Initial build
When first downloaded (or on an automated pipeline) the parent project probably will not be present.
Therefore knowing where to fetch the parent POM is required.
This value is held in the constant `deploy.aws.bucket` and in turn this value is in the parent project [`flightPOS.master`](https://bitbucket.org/backofficedev/flightpos.web.master). However at initial download the value is in (`mvn-settings.xml`).
So to do the inital build:
```
mvn --settings mvn-settings.xml compile
```

### Maven & AWS settings
To be sucessfull this will need an edit to `~/.m2/settings.xml` which must be updated to include access and secret keys for the account. This will be the account allocated to you by flightPOS.
The access key should be used to populate the username element, and the secret access key should be used to populate the password element.The access and secret keys for the account can be provided using
- `AWS_ACCESS_KEY_ID` (or `AWS_ACCESS_KEY`) and `AWS_SECRET_KEY` (or `AWS_SECRET_ACCESS_KEY`) environment variables
- `aws.accessKeyId` and `aws.secretKey` system properties




### Subsequent builds
After this it is only necessary to run:
```
mvn compile
```
### Build and deploy
To build the libary can be sent to AWS S3 using the `deploy` task. This needs the [AWS Wagon](https://github.com/spring-projects/aws-maven). This is added as an extension via the addition of an extension file in [.mvn](.mvn/readme.md).

